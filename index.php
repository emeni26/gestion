<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
</head>
<body>

<div class="container bg-info" >

<?php
 include ('conndb.php');

 $query = " SELECT * From user_table";
 $result = $db->query($query);

?>


<div class="row" style="margin-top:10px; margin-bottom: 10px">
<div class="col-sm-4"> 
<h4>Insert new user</h4>
<form action="insert.php" method="post">
  <div class="form-group">
    <label for="uname">User Name</label>
    <input type="text" class="form-control" name="uname">
  </div>
  <div class="form-group">
    <label for="umail">Email:</label>
    <input type="Email" class="form-control" name="uemail">
  </div>
  <div align="center">
  	 <button type="submit" class="btn btn-primary">Insert</button>

  </div>
 
</form>
</div>
<div class="col-sm-8">	
<h4> All users </h4>
<table class="table table-striped">
    <thead>
      <tr>
        <th>UserId</th>
        <th>User Name</th>
        <th>User Email</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
    <?php

while($row= $result -> fetchArray()){

    ?>

      <tr>
        <td><?php echo $row['user_id']   ; ?></td>
        <td><?php echo $row['user_name'] ; ?></td>
        <td><?php  echo $row['user_email']; ?></td>
        <td>


      
             <!-- update par id -->
           <a href="update.php?u_id=<?php echo $row ['user_id'] ; ?>" class="btn btn-info" role="button">Update</a>

             <!-- Suppression par id -->

           <a href="delete.php?u_id=<?php echo $row ['user_id'] ; ?>" class="btn btn-danger" role="button">Delete</a>

        </td>
      </tr>
   <?php

   	   }
   ?>
    </tbody>
  </table>

</div>
</div>


  
</div>
<script src="js/jquery.min.js"></script>
  <script src="Bootstrap/js/bootstrap.min.js"></script>
</body>
</html>